package com.example.tp3;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BlagueAdapter extends RecyclerView.Adapter<BlagueAdapter.MyViewHolder> {

    List<Blague> mesBlagues;

    BlagueAdapter(List<Blague> mesBlagues){

        this.mesBlagues = mesBlagues;

    }


    @Override
    public BlagueAdapter.MyViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recyclerview_blague,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.display(mesBlagues.get(position));
    }

    @Override
    public int getItemCount()  {
        return mesBlagues.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView tvQuestion;
        private TextView tvReponse;
        private  TextView tvsimpleBlague;


        MyViewHolder(View itemView ){
            super(itemView);

            tvQuestion = itemView.findViewById(R.id.tvQuestion);
            tvReponse = itemView.findViewById(R.id.tvReponse);
            tvsimpleBlague = itemView.findViewById(R.id.tvJoke);
        }

        void display (Blague blague){
            tvQuestion.setText(blague.getQuestion());
            tvReponse.setText(blague.getReponse());
            tvsimpleBlague.setText(blague.getSimpleBlague());
        }

    }

}
