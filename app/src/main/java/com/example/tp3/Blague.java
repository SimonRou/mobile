package com.example.tp3;

public class Blague {
    private String question;
    private String reponse;
    private String simpleBlague;

    Blague(String question , String reponse , String simpleBlague){
        this.question = question;
        this.reponse = reponse;
        this.simpleBlague = simpleBlague;
    }

    public String getQuestion() {return question;}
    public String getReponse() {return reponse;}
    public String getSimpleBlague() {return  simpleBlague;}
}
