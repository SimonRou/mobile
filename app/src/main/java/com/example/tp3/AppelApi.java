package com.example.tp3;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

public class AppelApi  extends AppCompatActivity {

    protected Button btn;
    protected Button btnSuppr;
    protected TextView tvQuestion;
    protected TextView tvReponse;

    protected TextView tvJoke;
    protected CheckBox cbNsfw;
    protected CheckBox cbReligieux;
    protected CheckBox cbPolitique;
    protected CheckBox cbRacist;
    protected CheckBox cbSexisme;
    protected List<Blague> mesBlagues;
    protected BlagueAdapter monAdapter;
    protected RecyclerView mRecyclerView ;
    protected Context mContext = this;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seconde_api);

        init_var();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init_req();
            }
        });

        btnSuppr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mesBlagues.clear();
                mRecyclerView.setAdapter(monAdapter);
            }
        });
    }

    private void init_var(){
        btn = findViewById(R.id.btn);
        btnSuppr = findViewById(R.id.btnSuppBlague);
        //tvQuestion = findViewById(R.id.tvQuestion);
        //tvReponse = findViewById(R.id.tvReponse);
        //tvJoke = findViewById(R.id.tvJoke);
        cbNsfw = findViewById(R.id.cbNsfw);
        cbPolitique = findViewById(R.id.cbPolitique);
        cbRacist = findViewById(R.id.cbRacisme);
        cbReligieux = findViewById(R.id.cbReligion);
        cbSexisme = findViewById(R.id.cbSexisme);
        mRecyclerView = findViewById(R.id.RecyclerView);

        mesBlagues = new ArrayList<>();

      //  mesBlagues.add(new Blague("zz","xx",""));


       // textView = findViewById(R.id.textView);
    }

    private void init_req(){
        Log.i("test" ,"Appel Bouton" );
        RequestQueue queue = Volley.newRequestQueue(this);
        List<String> ListeTags = new ArrayList<String>();

        int compteur = 0;
        String url = "";

        if (cbNsfw.isChecked()){
            ListeTags.add("nsfw");
            compteur++;
        }

        if (cbSexisme.isChecked()){
            ListeTags.add("sexist");
            compteur++;
        }

        if (cbReligieux.isChecked()){

            ListeTags.add("religious");
            compteur++;
        }

        if (cbPolitique.isChecked()){
            ListeTags.add("political");
            compteur++;
        }

        if (cbRacist.isChecked()){
            ListeTags.add("racist");
            compteur++;
        }

        if (compteur == 0) {
             url = "https://sv443.net/jokeapi/v2/joke/Any";
        }else{
            url = "https://sv443.net/jokeapi/v2/joke/Any?blacklistFlags=";
            String elements = ListeTags.get(ListeTags.size() - 1);
          //  Toast.makeText(this, "elements : " + elements , Toast.LENGTH_SHORT).show();
            for (int i = 0 ; i< ListeTags.size() ; i++ ){
                if (ListeTags.get(i) == elements){
                    url += ListeTags.get(i);

                }else{
                    url += ListeTags.get(i)+",";
                }

            }
        }

        //Toast.makeText(this, "Url : " + url , Toast.LENGTH_SHORT).show();
        Log.i("test" ,"Url : " + url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

                    String Question = "";
                    String Reponse = "";
                    String SimpleBlague = "";

                    public void onResponse(String response) {
                        // Do something with response
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);

                            Question = (String) jsonObject.get("setup");
                            Reponse = (String) jsonObject.get("delivery");

                            // Toast.makeText(AppelApi.this, Question, Toast.LENGTH_SHORT).show();
                            //  tvQuestion.setText(Question);


                        } catch (JSONException e) {
                            Log.i("test" ,"aie");
                            e.printStackTrace();
                          //  Toast.makeText(AppelApi.this, "Erreur : " + e, Toast.LENGTH_LONG).show();
                        }

                        try {
                            SimpleBlague = (String) jsonObject.get("joke");
                        } catch (JSONException e){
                            Log.i("test" ,"aie2");
                        }

                        Log.i("test" ,Question + " : " + Reponse + " : " + SimpleBlague);
                        mesBlagues.add(new Blague(Question,Reponse,SimpleBlague));

                        monAdapter = new BlagueAdapter(mesBlagues);
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                        mRecyclerView.setAdapter(monAdapter);

                        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration((mRecyclerView.getContext()),DividerItemDecoration.VERTICAL);
                        mRecyclerView.addItemDecoration(dividerItemDecoration);

                       // Toast.makeText(AppelApi.this, "Fin", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // Toast.makeText(AppelApi.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);

    }
}
