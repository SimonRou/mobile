package com.example.tp3;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    protected Button btnLorem;
    protected Button btnJoke;
    protected TextView textView;
    protected Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init_var();

        btnLorem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                init_req();
            }
        });

        btnJoke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,AppelApi.class);
                startActivity(intent);
            }
        });
    }

    private void init_var(){
        btnLorem = findViewById(R.id.btnLorem);
        btnJoke = findViewById(R.id.btn);
        textView = findViewById(R.id.textView);
    }

    private void init_req(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://baconipsum.com/api/?type=meat-and-filler&paras=5&format=text";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override

                    public void onResponse(String response) {
                        // Do something with response
                        //Toast.makeText(MainActivity.this, response, Toast.LENGTH_SHORT).show();
                        textView.setText(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);

    }

}
